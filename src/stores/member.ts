import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/MemBer'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    { id: 1, name: 'มานะ รักชาติ', tel: '0881234567' },
    { id: 2, name: 'มานี มีใจ', tel: '0887654321' }
  ])

  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }

  function clear() {
    currentMember.value = null
  }
  return { members, searchMember, currentMember, clear }
})
