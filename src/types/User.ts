type Gender = 'male' | 'female' | 'others'
type Role = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender //malm, female, Others
  roles: Role[] // admin, user
}

export type { Gender, Role, User }
